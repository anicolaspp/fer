//
//  TBundle.h
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TProduct.h"
#import "AutoCoding.h"



@interface TBundle : NSObject

@property (nonatomic, strong) NSMutableArray * Products;
@property (nonatomic, strong, readwrite) NSString * Description;



-(void)addProduct:(TProduct*)product;

-(void)setDescription:(NSString *)description;
-(NSString*)getDescription;

-(void)save;
+(TBundle *)loadFromDefaultLocation;

@end
