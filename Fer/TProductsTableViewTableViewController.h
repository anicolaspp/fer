//
//  TProductsTableViewTableViewController.h
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TProductsTableViewTableViewController : UITableViewController


@property (nonatomic, strong) NSMutableArray * products;


@end
