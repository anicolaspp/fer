//
//  TProductBundledTableViewController.h
//  Fer
//
//  Created by Nicolas A Perez on 6/23/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWTableViewCell.h"
#import "AutoCoding.h"

@interface TProductBundledTableViewController : UITableViewController<SWTableViewCellDelegate>


@property(nonatomic) int selectedValue;

@property (nonatomic, strong) NSMutableArray * bundles;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
