//
//  main.m
//  Fer
//
//  Created by Nicolas A Perez on 6/22/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([TAppDelegate class]));
	}
}
