//
//  TProductBundledTableViewController.m
//  Fer
//
//  Created by Nicolas A Perez on 6/23/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import "TProductBundledTableViewController.h"
#import "TProductsTableViewTableViewController.h"
#import "TProduct.h"
#import "TBundle.h"
#import "TBundleTableCell.h"
#import "Colours.h"
#import "TBundleList.h"

@interface TProductBundledTableViewController ()

@end

@implementation TProductBundledTableViewController


@synthesize selectedValue;
@synthesize bundles;
@synthesize tableView;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    [self.navigationItem setTitle: [[NSString alloc] initWithFormat: @"%d", selectedValue]];
    
    UIActivityIndicatorView *spinner = [self GetActivityIndicator];
   

    [self.view addSubview:spinner];
    [self.view setUserInteractionEnabled:NO];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    
    [spinner startAnimating];
    
    [self LoadBundlesAsync:spinner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [bundles count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"bundleCell";
    
    
    TBundleTableCell *cell = (TBundleTableCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = (TBundleTableCell*) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellIdentifier];
        
        
    }
    
    NSString * strForCell;
    
    strForCell = [[bundles objectAtIndex:indexPath.row] getDescription];
    
    [cell.textLabel setText:strForCell];
   
    cell.leftUtilityButtons = [self leftButtons];
    cell.delegate = self;
    
    // Configure the cell...
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"loadProducts" sender:self];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - WSTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    NSArray * rightutilityButtons = [self leftButtons];
    
    UIButton * buttonClicked = (UIButton*) [rightutilityButtons objectAtIndex:index];
    
    [[[UIAlertView alloc] initWithTitle:@"YEAH" message: buttonClicked.titleLabel.text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    
	// here is always returning the value 0
	NSInteger ind = [[tableView indexPathForCell:cell] row];
	
	[[tableView indexPathForCell:cell] row];
	
	NSLog(@"Selected Row: %ld", ind);

    TBundle * selectedBundle = [self.bundles objectAtIndex: ind];
    
	
    TBundleList * bundlesToSave = [TBundleList objectWithContentsOfFile:[[self class] documentPath]];
    [bundlesToSave addBundleObject: selectedBundle];

	[bundlesToSave writeToFile: [[self class] documentPath] atomically:YES];
	
    NSLog(@"Data saved");
    
}




#pragma Self

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"loadProducts"])
    {
        //set data here...
        
        TProductsTableViewTableViewController * dest = segue.destinationViewController;
        
        NSIndexPath * path = [tableView indexPathForSelectedRow];
        
        if (path)
        {
            NSInteger selectedRow = [path row];
            
            TBundle * bundle = [bundles objectAtIndex:selectedRow];
            
            dest.products = bundle.Products;
        }
    }

}


-(NSArray*)leftButtons
{
    NSMutableArray * rightUtilityButtons = [NSMutableArray new];

    UIColor * c = [UIColor colorFromRGBAArray:[[UIColor dangerColor] rgbaArray]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
      c title:@"Hide"];
    
    c = [UIColor colorFromRGBAArray:[[UIColor infoBlueColor] rgbaArray]];
    
     [rightUtilityButtons sw_addUtilityButtonWithColor:
      c title:@"Flag"];
     
    
    
    
    return rightUtilityButtons;
}

- (void)InitBundles
{
    [NSThread sleepForTimeInterval:5];
    
    bundles = [[NSMutableArray alloc] init];
    
    
    TProduct * product_1 = [[TProduct alloc] init];
    
    [product_1 setProductName:@"P1"];
    
    TProduct * product_2 = [[TProduct alloc] init];
    
    [product_2 setProductName:@"P2"];
    
    
    TBundle * bundle1 = [[TBundle alloc] init];
    [bundle1 setDescription:@"Best Choise"];
    
    [bundle1 addProduct: product_1];
    [bundle1 addProduct: product_2];
    
    TBundle * bundle2 = [[TBundle alloc] init];
    [bundle2 setDescription:@"Customers Choise"];
    
    [bundle2 addProduct: product_1];
    [bundle2 addProduct: product_2];
    
    bundles = [NSMutableArray arrayWithObjects: bundle1, bundle2, nil];
}

- (UIActivityIndicatorView *)GetActivityIndicator
{
    UIActivityIndicatorView * spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(160, 240);
    spinner.hidesWhenStopped = YES;
    return spinner;
}

- (void)LoadBundlesAsync:(UIActivityIndicatorView *)spinner
{
    dispatch_queue_t downloadQueque = dispatch_queue_create("Calculating", NULL);
    dispatch_async(downloadQueque, ^
                   {
                       //calculate bundles here...
                       [self InitBundles];
                       
                       dispatch_async(dispatch_get_main_queue(), ^
                                      {
                                          [spinner stopAnimating];
                                          [self.view setUserInteractionEnabled:YES];
                                          self.view.backgroundColor = [UIColor whiteColor];
                                          [tableView reloadData];
                                      });
                   });
}

@end
