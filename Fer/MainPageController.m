//
//  MainPageController.m
//  Fer
//
//  Created by Nicolas A Perez on 6/22/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import "MainPageController.h"
#import "TProductBundledTableViewController.h"

@interface MainPageController ()

@end

@implementation MainPageController

NSArray * _picketItems;


@synthesize _picker;
@synthesize _textUserValue;

@synthesize slider1;
@synthesize label1;

@synthesize slider2;
@synthesize label2;

@synthesize slider3;
@synthesize label3;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Init picker values
    _picketItems = [NSArray arrayWithObjects: @"Centimiters", @"BBB", @"CCC", nil];
   
    
    // Setting the keyboard up.
    
    _textUserValue.keyboardType = UIKeyboardTypeNumberPad;
    
    //setting keyboard aux buttons
    
    _textUserValue.inputAccessoryView = [self GetKeyBoardToolBar];
    
}



-(UIToolbar*) GetKeyBoardToolBar
{
    UIToolbar * numberToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 150, 50)];
    numberToolBar.barStyle = UIBarStyleDefault;
    numberToolBar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolBar sizeToFit];
    
    return numberToolBar;
}


-(void)doneWithNumberPad
{
    [_textUserValue resignFirstResponder];
}

-(void)cancelNumberPad
{
    [_textUserValue setText: @"0"];
    [_textUserValue resignFirstResponder];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationPortrait;
//}
//
//-(BOOL)shouldAutorotate
//{
//    return NO;
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch * touch = [[event allTouches] anyObject];
    
    if ([_textUserValue isFirstResponder] && [touch view] != _textUserValue)
    {
        [_textUserValue resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}



#pragma mark -
#pragma pickerView DataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_picketItems count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_picketItems objectAtIndex:row];
}

- (IBAction)_calculateBtn_Clicked:(id)sender {
    
//    [sender resignFirstResponder];
//    
//    int selectedRow = [_picker selectedRowInComponent: 0];
//    
//    NSString * item = [_picketItems objectAtIndex: selectedRow];
    
//    
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle: item message: @"YAH!" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles:nil];
//    
//    [alert show];
    
}

- (IBAction)textFieldReturn:(id)sender
{
    if ([_textUserValue.text isEqualToString:@""])
    {
        [_textUserValue setText:@"0"];
    }
    
    [sender resignFirstResponder];
}

- (IBAction)textFieldChanged:(id)sender
{
    if ([_textUserValue.text isEqualToString:@""])
    {
        [_textUserValue setText:@"0"];
    }
    else if ([[_textUserValue.text substringToIndex:1] isEqualToString:@"0"])
    {
        NSRange range;
        range.location = 1;
        range.length = [_textUserValue.text length] - 1;
        
        NSString * value = [_textUserValue.text substringWithRange:range];
        
         [_textUserValue setText:value];
    }
}

- (IBAction)textFieldDidBeginChange:(id)sender
{
    if ([_textUserValue.text isEqualToString:@"0"])
    {
        [_textUserValue setText:@""];
    }
}




- (IBAction)slider1_changed:(id)sender
{
    float value = [slider1 value];
    
    NSString * strValue = [[NSString alloc] initWithFormat:@"%3.0f", value];
    
    [label1 setText:strValue];
}


- (IBAction)slider2_changed:(id)sender
{
    float value = [slider2 value];
    
    NSString * strValue = [[NSString alloc] initWithFormat:@"%3.0f", value];
    
    [label2 setText: strValue];
}

- (IBAction)slider3_changed:(id)sender
{
    float value = [slider3 value];
    
    NSString * strValue = [[NSString alloc] initWithFormat:@"%3.0f", value];
    
    [label3 setText: strValue];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"productBundles"])
    {
        //set data here...
        
        TProductBundledTableViewController * dest = segue.destinationViewController;
        dest.selectedValue = [_textUserValue.text intValue];
        
    }
}

@end



































