//
//  TFavoritesBundlesTableViewController.h
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TBundleList.h"

@interface TFavoritesBundlesTableViewController : UITableViewController

@property(nonatomic, strong) TBundleList * bundles;

@end
