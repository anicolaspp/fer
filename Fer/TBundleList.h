//
//  TBundleList.h
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBundle.h"

@interface TBundleList : NSObject

@property (nonatomic, strong, readwrite) NSMutableArray * bundles;

-(void)addBundleObject:(TBundle *)object;

@end
