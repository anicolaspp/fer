//
//  TFavoritesBundlesTableViewController.m
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import "TFavoritesBundlesTableViewController.h"

#import "TBundleTableCell.h"


@interface TFavoritesBundlesTableViewController ()

@end

@implementation TFavoritesBundlesTableViewController


@synthesize bundles;


- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	bundles = [self loadFavoritesBundles];
}

// this reload the favorite list everytime we get into this view
-(void)viewDidAppear:(BOOL)animated
{
	bundles = [self loadFavoritesBundles];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	
	if (bundles.bundles == nil)
	{
		return 0;
	}
	
	return [bundles.bundles count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString * cellIdentifier = @"bundleCell";
	
	TBundleTableCell *cell = (TBundleTableCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
	
	if (cell == nil)
	{
		cell = (TBundleTableCell*) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellIdentifier];
		
		
	}
	
	NSString * strForCell;
	
	strForCell = [[bundles.bundles objectAtIndex:indexPath.row] getDescription];
	
	[cell.textLabel setText:strForCell];
	
		//cell.leftUtilityButtons = [self leftButtons];
		//cell.delegate = self;
	
		// Configure the cell...
	
	return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - Self

-(NSArray*)leftButtons
{
	return [NSArray new];
}

-(TBundleList*)loadFavoritesBundles
{
	TBundleList * result = [TBundleList objectWithContentsOfFile:[[self class] documentPath]];
	
	return result;
}

@end
