//
//  TProduct.m
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import "TProduct.h"

@implementation TProduct

@synthesize ProductName;


-(void)setProductName:(NSString *)productName
{
    ProductName = productName;
}

-(NSString*)getProductName
{
    return self->ProductName;
}

@end
