//
//  TBundleList.m
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import "TBundleList.h"

@implementation TBundleList

@synthesize bundles;

-(void)addBundleObject:(TBundle *)object
{
	if (bundles == nil)
	{
		bundles = [NSMutableArray new];
	}
	
	[bundles addObject:object];
}

@end
