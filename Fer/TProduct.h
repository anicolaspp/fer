//
//  TProduct.h
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TProduct : NSObject

@property (nonatomic, strong, readwrite) NSString * ProductName;

-(void)setProductName:(NSString *)productName;

-(NSString *)getProductName;

@end
