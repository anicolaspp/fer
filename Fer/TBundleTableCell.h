//
//  TBundleTableCell.h
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface TBundleTableCell : SWTableViewCell

@property(nonatomic, weak) UILabel * customLabel;
@property(nonatomic, weak) UIImageView * customImageView;

@end
