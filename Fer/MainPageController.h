//
//  MainPageController.h
//  Fer
//
//  Created by Nicolas A Perez on 6/22/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainPageController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *_picker;

@property (weak, nonatomic) IBOutlet UITextField *_textUserValue;
- (IBAction)textFieldReturn:(id)sender;
- (IBAction)textFieldChanged:(id)sender;
- (IBAction)textFieldDidBeginChange:(id)sender;


- (IBAction)_calculateBtn_Clicked:(id)sender;



@property (weak, nonatomic) IBOutlet UISlider *slider1;

- (IBAction)slider1_changed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *label1;


@property (weak, nonatomic) IBOutlet UISlider *slider2;

- (IBAction)slider2_changed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *label2;


@property (weak, nonatomic) IBOutlet UISlider *slider3;

- (IBAction)slider3_changed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *label3;

@end
