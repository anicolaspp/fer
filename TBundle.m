//
//  TBundle.m
//  Fer
//
//  Created by Nicolas A Perez on 6/24/14.
//  Copyright (c) 2014 Nicolas A Perez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBundle.h"

@implementation TBundle


@synthesize Products;
@synthesize Description;

-(void)addProduct:(TProduct *)product
{
    if (Products == NULL)
    {
        Products = [[NSMutableArray alloc] init];
    }
    
    [Products addObject: product];
}


-(void)setDescription:(NSString *)description
{
    Description = description;
}

-(NSString*)getDescription
{
    return Description;
}

-(void)save
{
	NSString *path = [[self class] documentPath];
	
	NSLog(path);
	
	[self writeToFile:path atomically:YES];
}

+(TBundle *)loadFromDefaultLocation
{
	NSString * path = [[self class] documentPath];
	
	NSLog(path);
	
	TBundle * result = [TBundle objectWithContentsOfFile: path];
	
	return result;
}


@end
